using Test

function limpar_string(string)
	string = lowercase(string)
	string_to_array = []
	index = 1
	while index <= sizeof(string)
		if !ispunct(string[index]) && !isspace(string[index])
			push!(string_to_array, string[index])
		end
		index = nextind(string, index)
	end
	vetorcomplexo = ['á', 'à', 'ã', 'â', 'ä',
					 'é', 'è', 'ẽ', 'ê', 'ë',
					 'í', 'ì', 'ĩ', 'î', 'ï',
					 'ó', 'ò', 'õ', 'ô', 'ö',
					 'ú', 'ù', 'ũ', 'û', 'ü']
	for i in 1:length(string_to_array)
		if string_to_array[i] ∈ vetorcomplexo[1:5]
			string_to_array[i] = 'a'
		elseif string_to_array[i] ∈ vetorcomplexo[6:10]
			string_to_array[i] = 'e'
		elseif string_to_array[i] ∈ vetorcomplexo[11:15]
			string_to_array[i] = 'i'
		elseif string_to_array[i] ∈ vetorcomplexo[16:20]
			string_to_array[i] = 'o'
		elseif string_to_array[i] ∈ vetorcomplexo[21:25]
			string_to_array[i] = 'u'
		end
	end
	return string_to_array
end

function palindromo(string)
	string_limpa = limpar_string(string)
	string_inversa = []
	for i in 1:length(string_limpa)
		pushfirst!(string_inversa, string_limpa[i])
	end
	return string_limpa == string_inversa
end

@test palindromo("") == true
@test palindromo("ovo") == true
@test palindromo("MiniEP11") == false
@test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
@test palindromo("A mãe te ama.") == true
@test palindromo("O duplo pó do trote torpe de potro meu que morto pede protetor todo polpudo.") == true
@test palindromo("Luza Rocelina, a namorada do Manuel, leu na moda da Romana: anil é cor azul.") == true
@test palindromo("Seco de ira, coloco no colo caviar e doces.") == false
@test palindromo("Violeta esse corpo, processe a dor!") == false

println("Tudo Ok!")
